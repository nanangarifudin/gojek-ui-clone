package com.android.gojekuiclone.ui.beranda

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.gojekuiclone.R
import com.android.gojekuiclone.data.DataDummy
import com.android.gojekuiclone.data.Promo
import com.android.gojekuiclone.data.QuickAccess
import com.android.gojekuiclone.databinding.FragmentBerandaBinding
import com.android.gojekuiclone.databinding.ItemPromoHomeBinding
import com.android.gojekuiclone.databinding.ItemQuickAccessBinding
import com.android.gojekuiclone.ui.MainActivity
import com.bumptech.glide.Glide

class BerandaFragment : Fragment() {
    private val binding by lazy { FragmentBerandaBinding.inflate(layoutInflater) }
    private var listPromo = arrayListOf<Promo>()
    private var listQuickAccess = arrayListOf<QuickAccess>()
    private lateinit var promoAdapter: PromoAdapter
    private lateinit var quickAccessAdapter: QuickAccessAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        promoAdapter = PromoAdapter()
        quickAccessAdapter = QuickAccessAdapter()
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            DataDummy.apply {
                addDataPromo()
                addDataQuickAccess()
            }
            DataDummy.listQuickAccess = DataDummy.listQuickAccess
            DataDummy.listPromo = DataDummy.listPromo
            rvPromo.apply {
                adapter = promoAdapter
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            }
            rvQuickAccess.apply {
                adapter = quickAccessAdapter
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            }
        }
    }

    inner class PromoAdapter : RecyclerView.Adapter<PromoAdapter.ViewHolderAdapter>() {
        inner class ViewHolderAdapter(val view: ItemPromoHomeBinding) :
            RecyclerView.ViewHolder(view.root)

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): ViewHolderAdapter {
            val binding =
                ItemPromoHomeBinding.inflate(LayoutInflater.from(context), parent, false)
            return ViewHolderAdapter(binding)
        }

        override fun onBindViewHolder(holder: ViewHolderAdapter, position: Int) {
            val data = listPromo[position]
            holder.view.apply {
                txtTitlePromo.text = data.title
                txtCaptionPromo.text = data.caption
                Glide.with(holder.itemView.context).load(listPromo[position].image)
                    .fitCenter()
                    .into(imgPromo)
            }
        }

        override fun getItemCount(): Int = listPromo.size
    }

    inner class QuickAccessAdapter : RecyclerView.Adapter<QuickAccessAdapter.ViewHolderAdapter>() {
        inner class ViewHolderAdapter(val view: ItemQuickAccessBinding) :
            RecyclerView.ViewHolder(view.root)

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): ViewHolderAdapter {
            val binding =
                ItemQuickAccessBinding.inflate(LayoutInflater.from(context), parent, false)
            return ViewHolderAdapter(binding)
        }

        override fun onBindViewHolder(holder: ViewHolderAdapter, position: Int) {
            val data = DataDummy.listQuickAccess[position]
            holder.view.apply {
                txtCaption.text = data.name
                Glide.with(holder.itemView.context).load(DataDummy.listQuickAccess[position].icon)
                    .fitCenter()
                    .into(imageView3)
            }
        }

        override fun getItemCount(): Int = listPromo.size
    }



}