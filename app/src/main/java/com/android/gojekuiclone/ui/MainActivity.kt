package com.android.gojekuiclone.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.android.gojekuiclone.R
import com.android.gojekuiclone.databinding.ActivityMainBinding
import com.android.gojekuiclone.ui.beranda.BerandaFragment
import com.android.gojekuiclone.ui.chat.ChatFragment
import com.android.gojekuiclone.ui.pesanan.PesananFragment
import com.android.gojekuiclone.ui.promo.PromoFragment


class MainActivity : AppCompatActivity(){
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.apply {
            loadFragment(BerandaFragment())
            bnMain.setOnItemSelectedListener { item ->
                when (item.itemId) {
                    R.id.home_menu -> loadFragment(BerandaFragment())
                    R.id.promo_menu -> loadFragment(PromoFragment())
                    R.id.pesanan_menu -> loadFragment(PesananFragment())
                    R.id.chat_menu -> loadFragment(ChatFragment())
                }
                true
            }
        }
    }

    private fun loadFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(binding.flContainer.id, fragment)
            .commitAllowingStateLoss()
    }

}