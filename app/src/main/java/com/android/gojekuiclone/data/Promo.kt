package com.android.gojekuiclone.data


data class Promo(
    var image: String,
    var title: String,
    var caption: String
)