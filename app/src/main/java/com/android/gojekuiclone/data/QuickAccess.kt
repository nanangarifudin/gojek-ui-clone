package com.android.gojekuiclone.data

data class QuickAccess(
    val name: String,
    val icon: Int
)