package com.android.gojekuiclone.data
import com.android.gojekuiclone.R

object DataDummy {
    var listPromo = arrayListOf<Promo>()
    var listQuickAccess = arrayListOf<QuickAccess>()
    fun addDataPromo() {
        listPromo.apply {
            add(
                Promo("https://lelogama.go-jek.com/post_thumbnail/promo-gocar-nov.jpg",
                    "Jalan Lebih Hemat","Jalan jalan lebih hemat gannn")
            )
            add(
                Promo("https://lelogama.go-jek.com/post_featured_image/voucher-go-jek-gratis-banner.jpg",
                    "Jalan Lebih Hemat","Jalan jalan lebih hemat gannn")
            )
            add(
                Promo("https://lelogama.go-jek.com/post_thumbnail/promo-gocar-nov.jpg",
                    "Jalan Lebih Hemat","Jalan jalan lebih hemat gannn")
            )
        }
    }

    fun addDataQuickAccess() {
        listQuickAccess.apply {
            add(QuickAccess("Quick access 1", R.drawable.ic_account))
            add(QuickAccess("Quick access 2", R.drawable.ic_account))
            add(QuickAccess("Quick access 3", R.drawable.ic_account))
        }
    }

}